package com.example.services;

import com.example.libros.LibrosClass;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface RetrofitService {
    @GET("N00198603/libros")
    Call<List<LibrosClass>> getALL();

    @POST("N00198603/libros")
    Call<LibrosClass> create(@Body LibrosClass libros);

}
