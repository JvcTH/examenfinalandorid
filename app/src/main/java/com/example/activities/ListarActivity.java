package com.example.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.adapter.LibroAdapter;
import com.example.afinal.R;
import com.example.libros.LibrosClass;
import com.example.services.RetrofitService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ListarActivity extends AppCompatActivity {
    private androidx.recyclerview.widget.RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar);

        recyclerView = findViewById(R.id.rvlibros);
        ImageView img = findViewById(R.id.imagen);
        TextView titulo = findViewById(R.id.tvTitulo);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://upn.lumenes.tk/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RetrofitService service = retrofit.create(RetrofitService.class);

        service.getALL().enqueue(new Callback<List<LibrosClass>>() {
            @Override
            public void onResponse(Call<List<LibrosClass>> call, Response<List<LibrosClass>> response) {
                if (response.code()==200){
                    List<LibrosClass> libros = response.body();
                    recyclerView.setAdapter(new LibroAdapter(libros));
                }else {
                    Log.i("MY_APP" , "falla en la app");
                }
            }

            @Override
            public void onFailure(Call<List<LibrosClass>> call, Throwable t) {
                Log.i("MY_APP" , "falla en el servidor");
            }
        });
    }
}