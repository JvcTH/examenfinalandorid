package com.example.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.afinal.MainActivity;
import com.example.afinal.R;
import com.example.libros.LibrosClass;
import com.example.services.RetrofitService;

import java.io.ByteArrayOutputStream;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CrearLibros extends AppCompatActivity {

    static final int REQUEST_PICK_FROM_GALLERY = 2;
    public String fileBase64;
    public ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_libros);

        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[]{ Manifest.permission.READ_EXTERNAL_STORAGE}, 1001);
        }

        Button btnGallery = findViewById(R.id.btnGaleria);
        Button btnEnviar = findViewById(R.id.btnEnviar);
        imageView = findViewById(R.id.galeria);
        EditText titulo = findViewById(R.id.etTitulo);
        EditText resumen = findViewById(R.id.etResumen);
        EditText autor = findViewById(R.id.etAutor);
        EditText fecha = findViewById(R.id.etFecha);
        EditText tienda1 = findViewById(R.id.etTienda1);
        EditText tienda2 = findViewById(R.id.etTienda2);
        EditText tienda3 = findViewById(R.id.etTienda3);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://upn.lumenes.tk/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RetrofitService service = retrofit.create(RetrofitService.class);

        btnGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                galleryAddPic();
            }
        });

        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LibrosClass libros = new LibrosClass();
                libros.setTitulo(titulo.getText().toString());
                libros.setAutor(autor.getText().toString());
                libros.setResumen(resumen.getText().toString());
                libros.setFecha_publicacion(fecha.getText().toString());
                libros.setImagen(fileBase64);
                libros.setTienda_1(tienda1.getText().toString());
                libros.setTienda_2(tienda2.getText().toString());
                libros.setTienda_3(tienda3.getText().toString());

                Call<LibrosClass> call = service.create(libros);

                call.enqueue(new Callback<LibrosClass>() {
                    @Override
                    public void onResponse(Call<LibrosClass> call, Response<LibrosClass> response) {
                        Intent intent = new Intent(CrearLibros.this, MainActivity.class);
                        startActivity(intent);
                    }
                    @Override
                    public void onFailure(Call<LibrosClass> call, Throwable t) {
                    }
                });
            }
        });
    }

    private void galleryAddPic() {
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, REQUEST_PICK_FROM_GALLERY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_PICK_FROM_GALLERY && resultCode == RESULT_OK){
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();
            imageView.setImageBitmap(BitmapFactory.decodeFile(picturePath));
            //Añadir a base64
            fileBase64 = gtFile(picturePath);
        }
    }
    public static String gtFile(String filePath){
        Bitmap bmp = null;
        ByteArrayOutputStream bos = null;
        byte[] bt = null;
        String encodeString = null;
        try{
            bmp = BitmapFactory.decodeFile(filePath);
            bos = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG,100, bos);
            bt = bos.toByteArray();
            encodeString = Base64.encodeToString(bt, Base64.NO_WRAP);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return encodeString;
    }
}