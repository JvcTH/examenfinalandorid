package com.example.adapter;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.activities.SincronizarAct;
import com.example.afinal.R;
import com.example.libros.LibrosClass;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.List;

public class LibroAdapter extends RecyclerView.Adapter<LibroAdapter.LibrosViewHolder>{

    private List<LibrosClass> listaLibros;

    public LibroAdapter(List<LibrosClass> listaLibros) {
        this.listaLibros = listaLibros;
    }

    @NonNull
    @Override
    public LibroAdapter.LibrosViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_libros, parent, false);
        LibrosViewHolder vh = new LibrosViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull LibroAdapter.LibrosViewHolder holder, int position) {
        TextView titulo = holder.itemView.findViewById(R.id.tvTitulo);
        ImageView imagen = holder.itemView.findViewById(R.id.imagen);

        LibrosClass libros = listaLibros.get(position);
        titulo.setText(libros.getTitulo());

        String url = "https://upn.lumenes.tk" + libros.getImagen();
        Log.i("MY_APP" , "falla en la app"+ libros.getTitulo());
        Picasso.get().load(url).into(imagen);

        Button verDetalle = holder.itemView.findViewById(R.id.verDetalle);

        verDetalle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(holder.itemView.getContext(), SincronizarAct.class);
                intent.putExtra("Pokemon", new Gson().toJson(libros));
                holder.itemView.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listaLibros.size();
    }

    // Por cada clase adapter, se necesita una clase ViewHolder
    public class LibrosViewHolder extends RecyclerView.ViewHolder {

        public LibrosViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}